#!/usr/bin/env python3
"""Builds packages"""
import sys
from pathlib import Path
from prebuilder.systems import CMake
from prebuilder.distros.debian import Debian
from prebuilder.buildPipeline import BuildPipeline, BuildRecipy
from prebuilder.repoPipeline import RepoPipelineMeta
from prebuilder.core.Package import PackageMetadata
from prebuilder.core.Package import PackageRef, VersionedPackageRef
from prebuilder.fetchers.GitRepoFetcher import GitRepoFetcher
from fsutilz import movetree, copytree
from ClassDictMeta import ClassDictMeta

thisDir = Path(".").absolute()


class build(metaclass=RepoPipelineMeta):
	"""It's {maintainerName}'s repo for {repoKind} packages of build tools."""
	
	DISTROS = (Debian,)
	
	def llvm_spirv():
		repoURI = "https://github.com/KhronosGroup/SPIRV-LLVM-Translator"
		
		bakeBuildRecipy = BuildRecipy(CMake, GitRepoFetcher(repoURI, refspec="master"), buildOptions = {"LLVM_SPIRV_INCLUDE_TESTS": False, "LLVM_SPIRV_BUILD_EXTERNAL": True, "LLVM_SPIRV_BUILD_EXTERNAL": True, "BUILD_SHARED_LIBS": True}, patches=[thisDir / "patches"])
		bakeMetadata = PackageMetadata("llvm-spirv")
		
		return BuildPipeline(bakeBuildRecipy, ((Debian, bakeMetadata),))


if __name__ == "__main__":
	build()
